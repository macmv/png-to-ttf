# PNG to TTF

Converts a PNG file to a TTF.

## How to run

Place the input named `in.png` in the current directory.

Run this command:

```
cargo run
```

Now `out.ttf` should exist in the current directory.
