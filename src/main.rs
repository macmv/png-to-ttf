mod load;
mod ttf;

use fonttools::tables::glyf::Point;
use image::{io::Reader as ImageReader, Rgba, RgbaImage};

pub struct Font {
  start:  u32,
  glyphs: Vec<Glyph>,
}
pub struct Glyph {
  pixels: RgbaImage,
}

const SEP: Rgba<u8> = Rgba([255, 0, 0, 255]);
const BLACK: Rgba<u8> = Rgba([0, 0, 0, 255]);

impl Font {
  pub fn load(start: u32, image: &RgbaImage) -> Self {
    let mut glyphs = vec![];
    let mut prev = 1;
    for x in 1..image.width() {
      let px = *image.get_pixel(x, 1);
      if px == SEP {
        glyphs.push(Glyph::load(prev, x, image));
        prev = x + 1;
      }
    }
    Font { start, glyphs }
  }

  pub fn len(&self) -> u16 { self.glyphs.len() as u16 }
  pub fn start(&self) -> u16 { self.start as u16 }
  pub fn end(&self) -> u16 { self.start() + self.len() }

  pub fn glyph(&self, id: u16) -> &Glyph { &self.glyphs[id as usize] }
}

impl Glyph {
  pub fn load(min_x: u32, max_x: u32, image: &RgbaImage) -> Glyph {
    let mut pixels = RgbaImage::new(max_x - min_x, image.height() - 2);
    for x in min_x..max_x {
      for y in 1..image.height() - 1 {
        pixels.put_pixel(x - min_x, y - 1, *image.get_pixel(x, y));
      }
    }
    Glyph { pixels }
  }

  pub fn to_contours(&self) -> Vec<Vec<Point>> {
    let mut points = vec![];
    for x in 0..self.pixels.width() {
      for y in 0..self.pixels.height() {
        if *self.pixels.get_pixel(x, y) == BLACK {
          let point = |x: u32, y: u32| -> Point {
            Point {
              x:        x as i16 * 100,
              y:        (self.pixels.height() - y) as i16 * 100,
              on_curve: true,
            }
          };
          points.push(vec![point(x, y), point(x + 1, y), point(x + 1, y + 1), point(x, y + 1)]);
        }
      }
    }
    points
  }
  pub fn width(&self) -> u16 { self.pixels.width() as u16 }
}

fn main() {
  // let font =
  // Font::load("/usr/share/fonts/iosevka-custom/iosevka-custom-regular.ttf").
  // unwrap(); dbg!(font.tables.hmtx()?.unwrap());

  let image = ImageReader::open("in.png").unwrap().decode().unwrap();
  let image = image.into_rgba8();
  let font = Font::load(0x20, &image);

  ttf::generate(&font, "out.ttf");

  // Make sure freetype can read our font
  load::load("out.ttf");
}
