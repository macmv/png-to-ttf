use super::Font as CustomFont;
use chrono::naive::NaiveDate;
use fonttools::{
  font::{Font, SfntVersion},
  tables,
  tables::{
    cmap::CmapSubtable,
    glyf::{Glyph, Point},
    hmtx::Metric,
    name::{NameRecord, NameRecordID},
  },
};
use std::collections::BTreeMap;

fn gdef_table(font: &CustomFont) -> tables::GDEF::GDEF {
  let mut glyph_class = BTreeMap::new();
  for i in font.start()..font.end() {
    glyph_class.insert(i, tables::GDEF::GlyphClass::BaseGlyph);
  }
  tables::GDEF::GDEF {
    glyph_class,
    attachment_point_list: BTreeMap::new(),
    ligature_caret_list: BTreeMap::new(),
    mark_attachment_class: BTreeMap::new(),
    mark_glyph_sets: None,
    item_variation_store: None,
  }
}
fn cmap_table(font: &CustomFont) -> tables::cmap::cmap {
  let mut mapping = BTreeMap::new();
  for i in font.start()..font.end() {
    mapping.insert(i as u32, i as u16 - font.start() + 2);
  }
  tables::cmap::cmap {
    subtables: vec![
      CmapSubtable {
        format:      4, // unicode plane
        platformID:  0, // unicode
        encodingID:  3, // ???
        languageID:  0, // ???
        mapping:     mapping.clone(),
        uvs_mapping: None,
      },
      CmapSubtable {
        format:      12, // unicode plane
        platformID:  0,  // unicode
        encodingID:  4,  // ???
        languageID:  0,  // ???
        mapping:     mapping.clone(),
        uvs_mapping: None,
      },
      CmapSubtable {
        format:      4, // unicode plane
        platformID:  3, // unicode
        encodingID:  1, // ???
        languageID:  0, // ???
        mapping:     mapping.clone(),
        uvs_mapping: None,
      },
      CmapSubtable {
        format: 12,     // unicode plane
        platformID: 3,  // unicode
        encodingID: 10, // ???
        languageID: 0,  // ???
        mapping,        // map from unicode to glyph ids
        uvs_mapping: None,
      },
    ],
  }
}
fn head_table() -> tables::head::head {
  tables::head::head {
    majorVersion:       1,
    minorVersion:       0,
    fontRevision:       1.0,
    checksumAdjustment: 3,
    magicNumber:        1594834165,
    flags:              15,
    unitsPerEm:         1000,
    created:            NaiveDate::from_ymd(2022, 8, 28).and_hms(0, 0, 0),
    modified:           NaiveDate::from_ymd(2022, 8, 28).and_hms(0, 0, 0),
    xMin:               0,
    yMin:               0,
    xMax:               1600,
    yMax:               1600,
    macStyle:           0,
    lowestRecPPEM:      0,
    fontDirectionHint:  2,
    indexToLocFormat:   1,
    glyphDataFormat:    0,
  }
}
fn hhea_table() -> tables::hhea::hhea {
  tables::hhea::hhea {
    majorVersion:        1,
    minorVersion:        0,
    ascender:            977,
    descender:           -205,
    lineGap:             68,
    advanceWidthMax:     1000,
    minLeftSideBearing:  -1000,
    minRightSideBearing: -1000,
    xMaxExtent:          1131,
    caretSlopeRise:      1,
    caretSlopeRun:       0,
    caretOffset:         0,
    reserved0:           0,
    reserved1:           0,
    reserved2:           0,
    reserved3:           0,
    metricDataFormat:    0,
    numberOfHMetrics:    6969,
  }
}
fn hmtx_table(font: &CustomFont) -> tables::hmtx::hmtx {
  let mut metrics = vec![];
  metrics.push(Metric { advanceWidth: 0, lsb: 0 });
  metrics.push(Metric { advanceWidth: 0, lsb: 0 });
  for i in 0..font.len() {
    metrics.push(Metric { advanceWidth: font.glyph(i).width() * 100 + 100, lsb: 0 });
  }
  tables::hmtx::hmtx { metrics }
}
fn glyf_table(font: &CustomFont) -> tables::glyf::glyf {
  let mut glyphs = vec![];
  let empty_glyph = Glyph {
    xMin:         0,
    xMax:         0,
    yMin:         0,
    yMax:         0,
    contours:     vec![],
    instructions: vec![],
    components:   vec![],
    overlap:      false,
  };
  glyphs.push(empty_glyph.clone()); // 0
  glyphs.push(empty_glyph.clone()); // 1
  for i in 0..font.len() {
    glyphs.push(Glyph {
      xMin:         0,
      xMax:         1000,
      yMin:         0,
      yMax:         1000,
      contours:     font.glyph(i).to_contours(),
      instructions: vec![],
      components:   vec![],
      overlap:      false,
    });
  }

  tables::glyf::glyf { glyphs }
}
fn maxp_table(font: &CustomFont) -> tables::maxp::maxp { tables::maxp::maxp::new05(font.len() + 2) }
fn name_table() -> tables::name::name {
  tables::name::name {
    records: vec![NameRecord::windows_unicode(
      NameRecordID::LicenseURL,
      "http://opensource.org/licenses/OFL-1.1",
    )],
  }
}

pub fn generate(font: &CustomFont, path: &str) {
  let mut out = Font::new(SfntVersion::TrueType);

  out.tables.insert(gdef_table(font));
  out.tables.insert(cmap_table(font));
  out.tables.insert(glyf_table(font));
  out.tables.insert(head_table());
  out.tables.insert(hhea_table());
  out.tables.insert(hmtx_table(font));
  out.tables.insert(maxp_table(font));
  out.tables.insert(name_table());

  out.save(path).unwrap();
}
